package com.antoniotari.reactiveampacheapp.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.facebook.network.connectionclass.ConnectionClassManager;
import com.facebook.network.connectionclass.ConnectionClassManager.ConnectionClassStateChangeListener;
import com.facebook.network.connectionclass.ConnectionQuality;
import com.facebook.network.connectionclass.DeviceBandwidthSampler;

import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antoniotari on 2017-03-22.
 */

public class ConnectionManager implements ConnectionClassStateChangeListener {

    public Observable<ConnectionQuality> getConnectionQuality() {
        ConnectionClassManager.getInstance().register(this);
        return Observable.create((OnSubscribe<ConnectionQuality>) subscriber -> {
            try {
                DeviceBandwidthSampler.getInstance().startSampling();
                Thread.sleep(10000);
                DeviceBandwidthSampler.getInstance().stopSampling();
                subscriber.onNext(ConnectionClassManager.getInstance().getCurrentBandwidthQuality());
                ConnectionClassManager.getInstance().remove(this);
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void onBandwidthStateChange(final ConnectionQuality bandwidthState) {
    }

    public static boolean hasNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return (haveConnectedWifi || haveConnectedMobile);
    }
}
