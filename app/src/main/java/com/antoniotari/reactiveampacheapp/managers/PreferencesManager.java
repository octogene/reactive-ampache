package com.antoniotari.reactiveampacheapp.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by antoniotari on 2017-03-22.
 */
public enum PreferencesManager {
    INSTANCE;

    private static final String KEY_PREFERENCES = "com.antoniotari.reactiveampacheapp.managers.shared.preferences";
    private static final String KEY_ENABLE_CACHE = "com.antoniotari.reactiveampacheapp.preferences.enablecache";
    private static final String KEY_DIALOG_NOT_SHOW = "com.antoniotari.reactiveampacheapp.preferences.donotshowdialog";
    private SharedPreferences mSharedPreferences;

    public void init(Context context) {
        mSharedPreferences = context.getSharedPreferences(KEY_PREFERENCES, Context.MODE_PRIVATE);
    }

    public void setCacheEnabled(final boolean enableCache) {
        mSharedPreferences.edit().putBoolean(KEY_ENABLE_CACHE, enableCache).commit();
    }

    public boolean isCacheEnabled() {
        return mSharedPreferences.getBoolean(KEY_ENABLE_CACHE,false);
    }

    public void toggleCacheEnabled() {
        setCacheEnabled(!isCacheEnabled());
    }

    public void setDoNotShowLoginDialog() {
        mSharedPreferences.edit().putBoolean(KEY_DIALOG_NOT_SHOW, true).commit();
    }

    public boolean shouldShowLoginDialog() {
        return !mSharedPreferences.getBoolean(KEY_DIALOG_NOT_SHOW,false);
    }
}
