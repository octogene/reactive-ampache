/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.utils;

import android.content.Context;
import android.os.Build;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.SocketException;
import java.net.SocketTimeoutException;

import com.antoniotari.reactiveampache.Exceptions.AmpacheApiException;
import com.antoniotari.reactiveampache.utils.AmpacheUtils;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.ui.views.PowerToast;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

/**
 * Created by antonio tari on 2016-05-29.
 */
public class Utils {

    public static void onError(Context context, Throwable throwable) {
        Crashlytics.logException(throwable);
        Log.error(throwable);
        String message;
        if (throwable instanceof AmpacheApiException) {
            AmpacheApiException ampacheApiException = (AmpacheApiException) throwable;
            message = "Ampache error\ncode:" + ampacheApiException.getAmpacheError().getCode() + "\nerror: " +
                    ((AmpacheApiException) throwable).getAmpacheError().getError();

            // 999 is undefined error
            if (ampacheApiException.getAmpacheError().getCode() != null
                    && ampacheApiException.getAmpacheError().getError() != null
                    && !ampacheApiException.isUndefinedCode()
                    && !ampacheApiException.isUndefinedErrorMessage()) {
                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
            }
        } else if (throwable.getLocalizedMessage() != null) {
            message = throwable.getLocalizedMessage();
        } else {
            message = "Undefined error";
        }
        Log.error(message);
    }

    public static boolean is400Error(Throwable throwable) {
        if (throwable == null) return false;
        if (!(throwable instanceof AmpacheApiException)) return false;
        AmpacheApiException ampacheApiException = (AmpacheApiException) throwable;
        if (ampacheApiException.getAmpacheError() == null) return false;
        String errorCode = ampacheApiException.getAmpacheError().getCode();
        return (!TextUtils.isEmpty(errorCode) &&
                TextUtils.isDigitsOnly(errorCode) &&
                Integer.parseInt(errorCode) > 400 &&
                Integer.parseInt(errorCode) < 600);
    }

    public static boolean isSocketException(Throwable throwable) {
        if (throwable == null) return false;
        if (throwable instanceof AmpacheApiException) {
            return isSocketException((AmpacheApiException)throwable);
        }
        return (throwable instanceof SocketTimeoutException) ||
                (throwable instanceof SocketException);
    }

    private static boolean isSocketException(AmpacheApiException throwable) {
        if (throwable == null) return false;
        if (throwable.getAmpacheError().getThrowable() == null) return false;
        return isSocketException(throwable.getAmpacheError().getThrowable());
    }

    public static boolean isConnectionError(Throwable throwable, Context context) {
        return  (isSocketException(throwable) || !AmpacheUtils.isInternetConnected(context));
    }

//    /**
//     * temporary fix to picasso issue with https
//     */
//    public static String httpsToHttp(String url) {
//        if (!TextUtils.isEmpty(url) && url.length() > 5 && url.substring(0, 5).equals("https")) {
//            return url.replace("https", "http");
//        }
//        return url;
//    }


    private static boolean isSpecialCharacter(int b) {
        if (b == 46) return false;
        if (b == 38) return false;
        if (b == 40) return false;
        if (b == 41) return false;
        if (b == 44) return false;
        if (b == 45) return false;

        if((b>32 && b<=47 )||(b>=58 && b<=64)||(b>=91 && b<=96) ||(b>=123 && b<=126)||b>126)
            return true;
        return false;
    }

    public static String sanitizeFileName(String source) {
        if (source.length() == 0) return source;
        source = source.replace("%20"," ");
        String[] reservedChars = {"?",":","\"","*","|","/","\\","<",">"};

        for (String str: reservedChars) {
            source = source.replace(str,"_");
        }

        //remove remaining special characters
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0;i<source.length();i++) {
            if(!isSpecialCharacter(source.charAt(i))) {
                stringBuilder.append(source.charAt(i));
            }
        }

        return stringBuilder.toString();
    }

    public static void setHtmlString(TextView textView, String string) {
        if (string == null) string = "";
        Spanned htmlString = null;
        try {
            htmlString = fromHtml(string);
            if (TextUtils.isEmpty(htmlString.toString())) {
                htmlString = null;
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        textView.setText(htmlString != null ? htmlString : string);
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (html == null) html = "";
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static void loadImage(String url, ImageView imageView) {
        if(imageView == null)return;
        if(imageView.getContext() == null)return;

        if(TextUtils.isEmpty(url)) {
            imageView.setImageResource(R.drawable.album_art_placeholder);
            return;
        }

        //String imageUrl = Utils.httpsToHttp(url);
        try {
            Picasso.get()
                    .load(url)
                    .error(R.drawable.album_art_placeholder)
                    .into(imageView);

//            Picasso.with(imageView.getContext())
//                    .load(url)
//                    .error(R.drawable.album_art_placeholder)
//                    .into(imageView);
        } catch (IllegalArgumentException e){
            Crashlytics.logException(e);
        }
    }

    public static Toast getWaitToast(Context context) {
        return new PowerToast(context.getApplicationContext(), R.string.wait_song);//Toast.makeText(viewHolder.mainCardView.getContext(),R.string.wait_song, Toast.LENGTH_LONG);
    }

    public static void showDialog(Context context, @StringRes final int title, @StringRes final int message, Runnable action) {
        showDialog(context, title, message, android.R.string.yes, android.R.string.cancel, action);
    }

    public static void showDialog(Context context, @StringRes final int title, @StringRes final int message,
            @StringRes final int actionTitle, @StringRes final int cancelActionTitle, Runnable action) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(actionTitle, (dialog, which) -> action.run())
                .setNegativeButton(cancelActionTitle, (dialog, which) -> {
                    // do nothing
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
