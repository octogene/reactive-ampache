/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.utils;

import java.util.HashMap;

import com.antoniotari.android.lastfm.LastFmBase;

/**
 * Created by antonio tari on 2016-06-19.
 */
public enum LastFmCache {
    INSTANCE;
    private final HashMap<String, LastFmBase> lastFmCache = new HashMap<>();

    public void putInCache(String key, LastFmBase lastFmArtist) {
        lastFmCache.put(key, lastFmArtist);
    }

    public void putInCache(String artistName, String albumName, LastFmBase lastFmArtist) {
        String key = artistName+albumName;
        lastFmCache.put(key, lastFmArtist);
    }

    public LastFmBase getFromCache(String artistName) {
        return lastFmCache.get(artistName);
    }

    public LastFmBase getFromCache(String artistName, String albumName) {
        return lastFmCache.get(artistName+albumName);
    }

    private void saveCacheToDisc() {

    }

    private HashMap<String, LastFmBase> getCacheFromDisc() {
        return null;
    }
}
