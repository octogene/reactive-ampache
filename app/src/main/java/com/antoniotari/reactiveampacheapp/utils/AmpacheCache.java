package com.antoniotari.reactiveampacheapp.utils;

import android.content.Context;
import android.os.Environment;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampache.models.Artist;
import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.FileUtil;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampache.utils.SerializeUtils;

import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;
import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by antoniotari on 2017-01-09.
 */

public enum AmpacheCache {
    INSTANCE;

    private OkHttpClient mOkHttpClient;

    public static final Executor SERIAL_EXECUTOR = Executors.newSingleThreadExecutor();

    private final Map<String,Artist> mArtistMap = new ArrayMap<>();
    private final Map<String,Album> mAlbumMap = new ArrayMap<>();
    private final Map<String,Song> mSongMap = new ArrayMap<>();
    private final Map<String,Playlist> mPlaylistMap = new ArrayMap<>();

    public void addArtistToCache(Artist artist) {
        mArtistMap.put(artist.getId(),artist);
    }

    public void addAlbumToCache(Album album) {
        mAlbumMap.put(album.getId(),album);
    }

    public void addPlaylistToCache(Playlist playlist) {
        mPlaylistMap.put(playlist.getName(), playlist);
    }

    public void addSongToCache(Song song) {
        mSongMap.put(song.getId(),song);
    }

    public Artist getArtist(String id) {
        return mArtistMap.get(id);
    }

    public Album getAlbum(String id) {
        return mAlbumMap.get(id);
    }

    public Song getSong(String id) {
        return mSongMap.get(id);
    }

    public Playlist getPlaylist(final String playlistName) {
        return mPlaylistMap.get(playlistName);
    }

    private OkHttpClient getOkHttpClient() {
        if (mOkHttpClient == null) {
            Builder bLong = new Builder()
                    .readTimeout(222, TimeUnit.SECONDS)
                    .writeTimeout(222, TimeUnit.SECONDS)
                    .connectTimeout(222, TimeUnit.SECONDS);
            mOkHttpClient = bLong.build();
        }
        return mOkHttpClient;
    }

    private String getFlatFilename(Song song) {
        return song.getArtist().getId() + "_" + song.getId();
    }

    private String getFileName(Context context, final Song song) {
        if (!canUseExternalStorage(context)) {
            return getFlatFilename(song);
        } else {
            // if using external storage there's a dir structure
            try {
                String name = song.getUrl().substring(song.getUrl().replace("\\u003d","=").indexOf("name=")).replace("name=","");
                name = Utils.fromHtml(Utils.sanitizeFileName(name)).toString();
                Log.blu(name);
                return name;
            } catch (Exception e){
                return song.getName()+".mp3";
            }
        }
    }

    private File getSaveDir(Context context, Song song) {
        if (canUseExternalStorage(context)) {
            File sdCard = Environment.getExternalStorageDirectory();
            String dirStr = String.format("%s/PowerAmpache/%s/%s", sdCard.getAbsolutePath(), song.getArtist().getName(), song.getAlbum().getName());
            File dir = new File(dirStr);
            dir.mkdirs();
            return dir;
        } else {
            return context.getFilesDir();
        }
    }

    private File getJsonSaveDir(Context context) {
        return context.getFilesDir();
    }

    public Observable<Song> downloadSong(Context context, final Song song) {
        //String filename = getFileName(song);
        final File file = getSongPath(context, song);
        return Observable.create((OnSubscribe<Song>) subscriber -> {
            try {
                Log.blu("before", song.getTitle());
                copyInputStreamToFile(downloadBinaryFile(song.getUrl()), file);
                Log.blu("after", song.getTitle());
                // writing song data to file
                FileUtil.getInstance().writeStringFile(context, getFlatFilename(song) + ".song",new SerializeUtils().toJsonString(song));
                subscriber.onNext(song);
                subscriber.onCompleted();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        }).subscribeOn(Schedulers.from(SERIAL_EXECUTOR)).observeOn(AndroidSchedulers.mainThread());
    }

    private InputStream downloadBinaryFile(String url) throws IOException {
        //url = java.net.URLEncoder.encode(url, "utf-8");
        Request request = new Request.Builder().url(url).build();
        Response response = getOkHttpClient().newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        return response.body().byteStream();
    }

    private void copyInputStreamToFile(InputStream in, File file) throws IOException {
        OutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        out.close();
        in.close();
    }

    public boolean isSongInCache(Context context, Song song) {
        File file = new File(getSaveDir(context, song), getFileName(context, song));
        return (file.exists());
    }

    public void deleteSongFromCache(Context context, Song song) {
        File fileData = new File(getJsonSaveDir(context), getFlatFilename(song)+".song");
        if(fileData.delete()) {
            File file = getSongPath(context, song);// new File(getSaveDir(context, song), getFileName(song));
            file.delete();
        }
    }

    public String getCachedSongPath(Context context, Song song) {
        File file = getSongPath(context, song);
        if (file.exists()) {
            return file.getPath();
        }
        return null;
    }

    private File getSongPath(Context context, Song song) {
        Log.blu(new SerializeUtils().toJsonString(song));
        return new File(getSaveDir(context, song), getFileName(context, song));
    }


    private List<Song> getCachedListSync(Context context) {
        SerializeUtils serializeUtils = new SerializeUtils();
        List<Song> songs = new ArrayList<>();
        Queue<File> files = new LinkedList<>();
        files.addAll(Arrays.asList(getJsonSaveDir(context).listFiles()));
        Log.log("antonio", files.size());

        while (!files.isEmpty()) {
            File file = files.remove();
            Log.log("antonio", file.getName());
            if (file.isDirectory()) {
                //files.addAll(Arrays.asList(file.listFiles()));
            } else if (file.getName().endsWith(".song")) {
                String jsonSong = FileUtil.getInstance().readStringFile(context, file.getName());
                if (!TextUtils.isEmpty(jsonSong)) {
                    Song song = serializeUtils.fromJson(jsonSong, Song.class);
                    songs.add(song);
                    Log.log("antonio", song.getTitle());
                }
            }
        }

        Collections.sort(songs, (Song o1, Song o2) -> {
            String title1 = o1.getArtist().getName() + o1.getTitle();
            String title2 = o2.getArtist().getName() + o2.getTitle();
            return title1.compareTo(title2);
        });

        return songs;
    }

    public Observable<List<Song>> getCachedList(Context context) {
        return Observable.create(new OnSubscribe<List<Song>>() {
            @Override
            public void call(final Subscriber<? super List<Song>> subscriber) {
                try {
                    subscriber.onNext(getCachedListSync(context));
                    subscriber.onCompleted();
                }catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Boolean> clearCache(Context context) {
        return Observable.create(new OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                List<Song> songs = getCachedListSync(context);
                for(Song song:songs) {
                    deleteSongFromCache(context,song);
                }
                subscriber.onNext(true);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }




    private boolean canUseExternalStorage(Context context) {
        if (!PermissionHandler.isWritePermissionGranted(context)) return false;

        boolean externalStorageAvailable = false;
        boolean externalStorageWriteable = false;
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            externalStorageAvailable = externalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            externalStorageAvailable = true;
            externalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            externalStorageAvailable = externalStorageWriteable = false;
        }

        return externalStorageAvailable && externalStorageWriteable ;
    }
}
