/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.fragments;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.models.Tag;
import com.antoniotari.reactiveampache.models.Tags;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager.OnPlaylistChangeListener;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;
import com.antoniotari.reactiveampacheapp.ui.adapters.PlaylistsAdapter;
import com.antoniotari.reactiveampacheapp.ui.adapters.PlaylistsAdapter.OnPlaylistClickListener;
import com.antoniotari.reactiveampacheapp.utils.AmpacheCache;

import rx.Observable;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.functions.Func1;

/**
 * Created by antonio tari on 2016-06-21.
 */
public class PlaylistsFragment extends BaseFragment implements OnPlaylistChangeListener {

    private PlaylistsAdapter mPlaylistsAdapter;
    private OnPlaylistClickListener mOnPlaylistClickListener;
    private TextView infoTextView;
    private List<Playlist> playlists;
    private List<Playlist> playlistsOriginal;
    private Subscription mSubscription;

    public PlaylistsFragment() {
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        if(activity instanceof OnPlaylistClickListener){
            mOnPlaylistClickListener = (OnPlaylistClickListener) activity;
        } else {
            throw new RuntimeException("activity MUST implement OnPlaylistClickListener");
        }

        PlaylistManager.INSTANCE.addOnPlaylistChangeListener(this);
        // listen for new songs to refresh the current playlist in case it changes
        mSubscription = PlayManager.INSTANCE.mSongSubject.subscribe(song -> onRefresh(), this::onError);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnPlaylistClickListener = null;
        PlaylistManager.INSTANCE.removeOnPlaylistChangeListener(this);
        if (mSubscription!=null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    protected void onRefresh() {
        AndroidObservable.bindFragment(this,
                AmpacheApi.INSTANCE.handshake()
                        .flatMap(handshakeResponse -> AmpacheApi.INSTANCE.getPlaylists()))
                .subscribe(playlistList -> {
                    super.resetFilterList();
                    initAdapter(playlistList);
                    stopWaiting();
                }, this::onError);
    }

    @Override
    protected void initialize() {
        AndroidObservable.bindFragment(this,
                AmpacheApi.INSTANCE.getPlaylists())
                .flatMap(new Func1<List<Playlist>, Observable<List<Playlist>>>() {
                    @Override
                    public Observable<List<Playlist>> call(final List<Playlist> playlists) {
                        return AmpacheCache.INSTANCE.getCachedList(getActivity())
                                .flatMap(new Func1<List<Song>, Observable<Playlist>>() {
                                    @Override
                                    public Observable<Playlist> call(final List<Song> songs) {
                                        playlists.add(0, PlaylistManager.INSTANCE.getOfflinePlaylist(songs));
                                        return Observable.from(playlists);
                                    }
                                })
                                .toList();
                    }
                })
                .subscribe(this::initAdapter, this::onError);
    }

    @Override
    protected void initViews(final View rootView) {
        super.initViews(rootView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        infoTextView = (TextView) rootView.findViewById(R.id.info_text_view);
    }

    private void addCurrentPlaylist(List<Playlist> playlists) {
        List<Song> songs = PlayManager.INSTANCE.getCurrentPlaylist();
        if (songs != null && !songs.isEmpty()) {
            playlists.add(0, new LocalPlaylist(songs, getString(R.string.current_playlist)));
        }
    }

//    private void addOfflinePlaylist(List<Playlist> playlists) {
//        List<Song> songs = AmpacheCache.INSTANCE.getCachedList(getContext().getApplicationContext());
//        if (songs != null && !songs.isEmpty()) {
//            playlists.add(0, PlaylistManager.INSTANCE.getOfflinePlaylist());
//        }
//    }

    private void initAdapter(List<Playlist> playlists) {
        if (playlists == null) {
            playlists = new ArrayList<>();
        }

        // add offline playlist
        //addOfflinePlaylist(playlists);
        // add the current playlist on top
        addCurrentPlaylist(playlists);

        playlists.addAll(PlaylistManager.INSTANCE.generateLocalPlaylists());

        this.playlists = playlists;

        // once we have the album list determine if fast scroll is needed
        mFastScrollWrapper.determineSetFastScroll(playlists.size());

        if(playlists==null || playlists.isEmpty()){
            infoTextView.setVisibility(View.VISIBLE);
            infoTextView.setText(R.string.empty_playlists);
        } else {
            infoTextView.setVisibility(View.GONE);
        }

        stopWaiting();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (mPlaylistsAdapter == null) {
            mPlaylistsAdapter = new PlaylistsAdapter(playlists);
            mPlaylistsAdapter.setOnPlaylistClickListener(mOnPlaylistClickListener);
            recyclerView.setAdapter(mPlaylistsAdapter);
        } else {
            mPlaylistsAdapter.setPlaylists(playlists);
            mPlaylistsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onError(final Throwable throwable) {
        super.onError(throwable);
        if (mPlaylistsAdapter == null || mPlaylistsAdapter.getItemCount() <= 0) {
            String errorStr = getString(R.string.empty_playlists) + getString(R.string.error_on_playlists);
            infoTextView.setVisibility(View.VISIBLE);
            infoTextView.setText(errorStr);
        }
    }

    @Override
    public void onNewPlaylist(final String name) {
        onRefresh();
    }

    @Override
    public void onPlaylistChanged(final String name) {
        onRefresh();
    }

    @Override
    public void onPlaylistRemoved(final String name) {
        onRefresh();
    }

    @Override
    public void filterList(final Tag tag) {
        super.filterList(tag);
        playlistsOriginal = new ArrayList<>(playlists);
        List<Playlist> filterPlay = Tags.filterListByTag(playlistsOriginal, tag.getId());
        initAdapter(filterPlay);
    }

    @Override
    public void resetFilterList() {
        super.resetFilterList();
        if (playlistsOriginal == null) return;
        initAdapter(playlistsOriginal);
    }
}
