/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.activities;

import android.app.Activity;
import android.content.Intent;

import java.util.List;

import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampacheapp.utils.AmpacheCache;

/**
 * Created by antonio tari on 2016-07-24.
 */
public class SearchResultActivity extends PlaylistActivity {

    public static final String KEY_INTENT_PLAYLIST_NAME = "com.antoniotari.reactiveampacheapp.playlistactivity.search.name";

    @Override
    protected void initAdapter(List<Song> songList) {
        // use the remote playlist method so we have the + button to add the songs to playlist
        initAdapterForRemotePlaylist(songList);
    }

    @Override
    protected Playlist getPlaylistData() {
        return AmpacheCache.INSTANCE.getPlaylist(getIntent().getStringExtra(KEY_INTENT_PLAYLIST_NAME));
    }

    public static void startSearchResultActivity(Activity activity, Playlist localPlaylist) {
        // start playlist activity
        Intent intent = new Intent(activity, SearchResultActivity.class);
        intent.putExtra(KEY_INTENT_PLAYLIST_NAME, localPlaylist.getName());
        //intent.putExtra(PlaylistActivity.KEY_INTENT_PLAYLIST, localPlaylist);
        AmpacheCache.INSTANCE.addPlaylistToCache(localPlaylist);
        activity.startActivity(intent);
    }
}
