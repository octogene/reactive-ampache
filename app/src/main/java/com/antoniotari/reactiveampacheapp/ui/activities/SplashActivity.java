/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.api.AmpacheSession;
import com.antoniotari.reactiveampacheapp.MainActivity;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.utils.Utils;
import com.crashlytics.android.Crashlytics;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        // select navigation
        if (AmpacheSession.INSTANCE.isUserAuthenticated()) {
            AmpacheApi.INSTANCE.initUser()
                    .subscribe(aVoid -> goToMain(),
                            throwable -> {
                                Crashlytics.logException(throwable);
                                Utils.onError(this, throwable);
                                goToLogin();
                            });
        } else {
            goToLogin();
        }
    }

    private void goToLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void goToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
