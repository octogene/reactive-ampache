package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampacheapp.ui.adapters.dragdrop.ItemTouchHelperAdapter;
import com.antoniotari.reactiveampacheapp.ui.adapters.dragdrop.OnStartDragListener;

/**
 * Created by antoniotari on 2016-10-18.
 */
public class LocalPlaylistAdapter extends SongsAdapter implements ItemTouchHelperAdapter {

    public interface OnSongMovedListener {
        void onSongMoved(int fromPosition, int toPosition, List<Song> newList);
    }

    private final OnStartDragListener mDragStartListener;
    private OnSongMovedListener mOnSongMovedListener;


    public LocalPlaylistAdapter(List<Song> songs, final OnRemoveToPlaylistClickListener onRemoveToPlaylistClickListener,
            OnSongMenuClickListener songMenuClickListener, OnStartDragListener dragStartListener) {
        super(songs, onRemoveToPlaylistClickListener, songMenuClickListener);
        mDragStartListener = dragStartListener;
    }

    public void setOnSongMovedListener(final OnSongMovedListener onSongMovedListener) {
        mOnSongMovedListener = onSongMovedListener;
    }

    @Override
    public SongViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int i) {
        SongViewHolder songViewHolder = super.onCreateViewHolder(viewGroup, i);
        songViewHolder.dragView.setVisibility(View.VISIBLE);
        return songViewHolder;
    }

    @Override
    public void onBindViewHolder(final SongViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        // Start a drag whenever the handle view it touched
        holder.dragView.setOnTouchListener((v, event) -> {
            if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                mDragStartListener.onStartDrag(holder);
            }
            return false;
        });
    }

    @Override
    public void onItemDismiss(int position) {
        // moved to the listener
        //mSongList.remove(position);
        //notifyItemRemoved(position);

        // fire the listener
        if (mOnSongMovedListener!=null) {
            mOnSongMovedListener.onSongMoved(position, -1, mSongList);
        }
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mSongList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);

        // fire the listener
        if (mOnSongMovedListener!=null) {
            mOnSongMovedListener.onSongMoved(fromPosition, toPosition, mSongList);
        }
        return true;
    }
}