package com.antoniotari.reactiveampacheapp.ui.activities;

import android.app.Activity;
import android.content.Intent;

import java.util.List;

import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampacheapp.managers.PlaylistManager;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;

/**
 * Created by antoniotari on 2017-04-20.
 */

public class OfflinePlaylistActivity extends PlaylistActivity {

    @Override
    protected void loadSongs(Playlist playlist) {
        initSongList(((LocalPlaylist)playlist).getSongList());
    }

    @Override
    protected boolean isLocalPlaylist() {
        return false;
    }

    @Override
    protected void onRefresh() {
        swipeLayout.setRefreshing(false);
    }

    public static Intent createIntent(Activity activity, List<Song> songsList) {
        LocalPlaylist playlist = PlaylistManager.INSTANCE.getOfflinePlaylist(songsList);
        return createIntent(activity, playlist);
    }

    public static Intent createIntent(Activity activity, LocalPlaylist playlist) {
        Intent intent = new Intent(activity, OfflinePlaylistActivity.class);
        intent.putExtra(PlaylistActivity.KEY_INTENT_PLAYLIST, playlist);
        return intent;
    }
}
