package com.antoniotari.reactiveampacheapp.listeners;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.PlaySongBehaviour;
import com.antoniotari.reactiveampacheapp.managers.ConnectionManager;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.managers.PreferencesManager;
import com.antoniotari.reactiveampacheapp.utils.AmpacheCache;
import com.antoniotari.reactiveampacheapp.utils.Utils;
import com.facebook.network.connectionclass.ConnectionQuality;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by antoniotari on 2017-03-22.
 */

public class SongSelectedListener extends PlaySongBehaviour implements OnClickListener {

    public SongSelectedListener(Song song, List<Song> songList) {
        super(song, songList);
    }

    @Override
    public void onClick(final View v) {
        onSongClick(v, Utils.getWaitToast(v.getContext()));
    }

    private void onSongClick(View view, final Toast waitToast) {
        waitToast.show();
        // some times the play action freezes the ui for a few milliseconds
        // post delay the play action to allow the ripple effect to take place
        // it also gives some time to the toast to show
        new Handler(Looper.getMainLooper()).postDelayed( () -> playSong(view.getContext()), 400);
    }
}
